/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

// Global Namespace
var cryozonic =
{
    avsFields: null,

    toggleSubscription: function(subscriptionId, edit)
    {
        var section = document.getElementById(subscriptionId);
        if (!section) return;

        if (cryozonic.hasClass(section, 'show'))
        {
            cryozonic.removeClass(section, 'show');
            if (edit) cryozonic.removeClass(section, 'edit');
        }
        else
        {
            cryozonic.addClass(section, 'show');
            if (edit) cryozonic.addClass(section, 'edit');
        }

        return false;
    },

    editSubscription: function(subscriptionId)
    {
        var section = document.getElementById(subscriptionId);
        if (!section) return;

        if (!cryozonic.hasClass(section, 'edit'))
            cryozonic.addClass(section, 'edit');
    },

    cancelEditSubscription: function(subscriptionId)
    {
        var section = document.getElementById(subscriptionId);
        if (!section) return;

        cryozonic.removeClass(section, 'edit');
    },

    hasClass: function(element, className)
    {
        return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
    },

    removeClass: function (element, className)
    {
        if (element.classList)
            element.classList.remove(className);
        else
        {
            var classes = element.className.split(" ");
            classes.splice(classes.indexOf(className), 1);
            element.className = classes.join(" ");
        }
    },

    addClass: function (element, className)
    {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += (' ' + className);
    },

    // Admin

    initRadioButtons: function()
    {
        // Switching between saved cards and new card
        var i, inputs = document.querySelectorAll('#saved-cards input');

        for (i = 0; i < inputs.length; i++)
            inputs[i].onclick = cryozonic.useCard;

        // Switching between new subscription and switch subsctiption
        inputs = document.querySelectorAll('#payment_form_cryozonic_stripe_subscription input.select.switch');

        for (i = 0; i < inputs.length; i++)
            inputs[i].onclick = cryozonic.switchSubscription;

        // Selecting a subscription from the dropdown
        var input = $('cryozonic_stripe_select_subscription');
        if (input)
            input.onchange = cryozonic.switchSubscriptionSelected;
    },

    disableStripeInputValidation: function()
    {
        var i, inputs = document.querySelectorAll(".stripe-input");
        for (i = 0; i < inputs.length; i++)
            $(inputs[i]).removeClassName('required-entry');
    },

    enableStripeInputValidation: function()
    {
        var i, inputs = document.querySelectorAll(".stripe-input");
        for (i = 0; i < inputs.length; i++)
            $(inputs[i]).addClassName('required-entry');
    },

    // Triggered when the user clicks a saved card radio button
    useCard: function(evt)
    {
        var parentId = 'payment_form_cryozonic_stripe_payment';

        // User wants to use a new card
        if (this.value == 'new_card')
        {
            $(parentId).addClassName("stripe-new");
            cryozonic.enableStripeInputValidation();
        }
        // User wants to use a saved card
        else
        {
            $(parentId).removeClassName("stripe-new");
            cryozonic.disableStripeInputValidation();
        }
    },

    switchSubscription: function(evt)
    {
        var newSubscriptionSection = 'payment_form_cryozonic_stripe_payment';
        var existingSubscriptionSection = 'select_subscription';
        var elements = $(existingSubscriptionSection).select( 'select', 'input');

        if (this.value == 'switch')
        {
            $(newSubscriptionSection).addClassName("hide");
            $(existingSubscriptionSection).removeClassName("hide");
            for (var i = 0; i < elements.length; i++)
            {
                if (!cryozonic.hasClass(elements[i], 'hide'))
                    elements[i].disabled = false;
            }
            cryozonic.disableStripeInputValidation();
        }
        else
        {
            $(newSubscriptionSection).removeClassName("hide");
            $(existingSubscriptionSection).addClassName("hide");
            cryozonic.enableStripeInputValidation();
        }
    },

    switchSubscriptionSelected: function(evt)
    {
        var id = 'switch_subscription_date_' + this.value;
        var inputs = $('cryozonic_stripe_subscription_start_date_control').select('input');
        for (var i = 0; i < inputs.length; i++)
        {
            if (inputs[i].id == id)
            {
                $(inputs[i]).removeClassName("hide");
                inputs[i].disabled = false;
            }
            else
            {
                $(inputs[i]).addClassName("hide");
                inputs[i].disabled = true;
            }
        }
    },

    initPaymentFormValidation: function()
    {
        // Adjust validation if necessary
        var hasSavedCards = document.getElementById('new_card');

        if (hasSavedCards)
        {
            var paymentMethods = document.getElementsByName('payment[method]');
            for (var j = 0; j < paymentMethods.length; j++)
                paymentMethods[j].addEventListener("click", cryozonic.toggleValidation);
        }
    },

    toggleValidation: function(evt)
    {
        $('new_card').removeClassName('validate-one-required-by-name');
        if (evt.target.value == 'cryozonic_stripe')
            $('new_card').addClassName('validate-one-required-by-name');
    },

    initMultiplePaymentMethods: function()
    {
        var wrappers = document.getElementsByClassName('admin__payment-method-wapper');
        var countPaymentMethods = wrappers.length;
        if (countPaymentMethods < 2) return;

        var form = document.getElementById('payment_form_cryozonic_stripe');
        this.addClass(form, 'indent');
        form.style.display = 'none';
    },

    placeAdminOrder: function()
    {
        var radioButton = document.getElementById('p_method_cryozonic_stripe');
        if (radioButton && !radioButton.checked)
            return order.submit();

        createStripeToken(function(err)
        {
            if (err)
                alert(err);
            else
                order.submit();
        });
    },

    initAdminStripeJs: function()
    {
        // Stripe.js intercept when placing a new order
        var btn = document.getElementById('order-totals');
        if (btn) btn = btn.getElementsByTagName('button');
        if (btn && btn[0]) btn = btn[0];
        if (btn) btn.onclick = cryozonic.placeAdminOrder;

        var topBtn = document.getElementById('submit_order_top_button');
        if (topBtn) topBtn.onclick = cryozonic.placeAdminOrder;
    },

    setAVSFieldsFrom: function(billingAddress)
    {
        var street = [];

        // Mageplaza OSC delays to set the street because of Google autocomplete,
        // but it does set the postcode correctly, so we temporarily ignore the street
        if (billingAddress.street && billingAddress.street.length > 0)
            street = billingAddress.street;

        cryozonic.avsFields = {
            address_line1: (street.length > 0 ? street[0] : null),
            address_line2: (street.length > 1 ? street[1] : null),
            address_city: billingAddress.city || null,
            address_state: billingAddress.region || null,
            address_zip: billingAddress.postcode || null,
            address_country: billingAddress.countryId || null
        };
    },

    addAVSFieldsTo: function(cardDetails)
    {
        if (cryozonic.avsFields)
            jQuery.extend(cardDetails, cryozonic.avsFields);

        return cardDetails;
    },

    // Triggered from the My Saved Cards section
    saveCard: function(saveButton)
    {
        saveButton.disabled = true;

        createStripeToken(function(err)
        {
            if (err)
            {
                alert(err);
                saveButton.disabled = false;
            }
            else
                document.getElementById('payment_form_cryozonic_stripe_payment').submit();
        });

        return false;
    },

    getCardDetails: function()
    {
        // Validate the card
        var cardName = document.getElementById('cryozonic_stripe_cc_owner');
        var cardNumber = document.getElementById('cryozonic_stripe_cc_number');
        var cardCvc = document.getElementById('cryozonic_stripe_cc_cid');
        var cardExpMonth = document.getElementById('cryozonic_stripe_expiration');
        var cardExpYear = document.getElementById('cryozonic_stripe_expiration_yr');

        var isValid = cardName && cardName.value && cardNumber && cardNumber.value && cardCvc && cardCvc.value && cardExpMonth && cardExpMonth.value && cardExpYear && cardExpYear.value;

        if (!isValid) return null;

        var cardDetails = {
            name: cardName.value,
            number: cardNumber.value,
            cvc: cardCvc.value,
            exp_month: cardExpMonth.value,
            exp_year: cardExpYear.value
        };

        cardDetails = cryozonic.addAVSFieldsTo(cardDetails);

        return cardDetails;
    }
};

initAdminEvents = function()
{
    cryozonic.initRadioButtons();
    cryozonic.initPaymentFormValidation();
    cryozonic.initMultiplePaymentMethods();
};

var stripeTokens = {};

var initStripe = function(apiKey, callback)
{
    if (typeof callback == "undefined")
        callback = function() {};

    // Load Stripe.js dynamically
    if (typeof Stripe == "undefined")
    {
        var resource = document.createElement('script');
        resource.src = "https://js.stripe.com/v2/";
        resource.onload = function() {
            Stripe.setPublishableKey(apiKey);
            callback();
        };
        resource.onerror = function(err) {
            callback("Stripe.js could not be loaded: " + err);
            throw new Error("Stripe.js could not be loaded: " + err);
        };
        var script = document.getElementsByTagName('script')[0];
        script.parentNode.insertBefore(resource, script);
    }
    else
        Stripe.setPublishableKey(apiKey);
};

var createStripeToken = function(done)
{
    // First check if the "Use new card" radio is selected, return if not
    var newCardRadio = document.getElementById('new_card');
    if (newCardRadio && !newCardRadio.checked) return done();

    // Check if we are switching from another subscription, return if we are
    var switchSubscription = document.getElementById('switch_subscription');
    if (switchSubscription && switchSubscription.checked) return done();

    var cardDetails = cryozonic.getCardDetails();

    if (!cardDetails)
        return done('Invalid card details');

    var cardKey = JSON.stringify(cardDetails);

    if (stripeTokens[cardKey])
    {
        setStripeToken(stripeTokens[cardKey]);
        return done(null, stripeTokens[cardKey]);
    }

    Stripe.card.createToken(cardDetails, function (status, response)
    {
        if (response.error)
        {
            if (typeof IWD != "undefined")
            {
                IWD.OPC.Checkout.hideLoader();
                IWD.OPC.Checkout.xhr = null;
                IWD.OPC.Checkout.unlockPlaceOrder();
            }
            done(response.error.message);
        }
        else
        {
            var token = response.id + ':' + response.card.brand + ':' + response.card.last4;
            stripeTokens[cardKey] = token;
            setStripeToken(token);
            done(null, token);
        }
    });
};

function setStripeToken(token)
{
    try
    {
        var input, inputs = document.getElementsByClassName('cryozonic-stripejs-token');
        if (inputs && inputs[0]) input = inputs[0];
        else input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "payment[cc_stripejs_token]");
        input.setAttribute("class", 'cryozonic-stripejs-token');
        input.setAttribute("value", token);
        input.disabled = false; // Gets disabled when the user navigates back to shipping method
        var form = document.getElementById('payment_form_cryozonic_stripe_payment');
        if (!form) form = document.getElementById('co-payment-form');
        if (!form) form = document.getElementById('order-billing_method_form');
        if (!form) form = document.getElementById('onestepcheckout-form');
        if (!form && typeof payment != 'undefined') form = document.getElementById(payment.formId);
        if (!form)
        {
            form = document.getElementById('new-card');
            input.setAttribute("name", "newcard[cc_stripejs_token]");
        }
        form.appendChild(input);
    } catch (e) {}
}

// Multi-shipping form support for Stripe.js
var multiShippingForm = null, multiShippingFormSubmitButton = null;

function submitMultiShippingForm(e)
{
    if (payment.currentMethod != 'cryozonic_stripe')
        return true;

    if (e.preventDefault) e.preventDefault();

    if (!multiShippingFormSubmitButton) multiShippingFormSubmitButton = document.getElementById('payment-continue');
    if (multiShippingFormSubmitButton) multiShippingFormSubmitButton.disabled = true;

    createStripeToken(function(err)
    {
        if (err)
            alert(err);
        else
        {
            if (multiShippingFormSubmitButton) multiShippingFormSubmitButton.disabled = false;
            multiShippingForm.submit();
        }
    });

    return false;
}

// Multi-shipping form
var initMultiShippingForm = function()
{
    if (typeof payment == 'undefined' || payment.formId != 'multishipping-billing-form') return;

    multiShippingForm = document.getElementById(payment.formId);
    if (!multiShippingForm) return;

    if (multiShippingForm.attachEvent)
        multiShippingForm.attachEvent("submit", submitMultiShippingForm);
    else
        multiShippingForm.addEventListener("submit", submitMultiShippingForm);
};
