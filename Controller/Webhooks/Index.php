<?php
/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

namespace Cryozonic\StripePayments\Controller\Webhooks;

use Cryozonic\StripePayments\Helper\Logger;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Cryozonic\StripePayments\Helper\Generic $helper,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $dbTransaction
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);

        $this->helper = $helper;
        $this->invoiceService = $invoiceService;
        $this->dbTransaction = $dbTransaction;
    }

    public function paymentSucceeded($event)
    {
        if (!empty($event->data->object->lines->data[0]))
            $subscription = $event->data->object->lines->data[0];

        if (!$subscription)
            throw new \Exception("Received {$event->type} webhook but could not read the subscription object.");

        $metadata = $subscription->metadata;

        if (!empty($metadata->{'Order #'}))
            $orderId = $metadata->{'Order #'};
        else
            throw new \Exception("The webhook request has no Order ID in its metadata - ignoring.");

        if (!empty($metadata->{'Product ID'}))
            $productId = $metadata->{'Product ID'};
        else
            throw new \Exception("The webhook request has no product ID in its metadata - ignoring.");

        $transactionId = $event->data->object->charge;
        $currency = $event->data->object->currency;
        $orderItemId = false;
        $markAsPaid = true;

        if (isset($event->data->object->total))
            $amountPaid = $event->data->object->total;
        else
            $amountPaid = $subscription->amount;

        if ($amountPaid <= 0)
            return;

        $cents = 100;
        $decimals = 2;
        if ($this->helper->isZeroDecimal($currency))
        {
            $cents = 1;
            $decimals = 0;
        }

        $amountPaid = round($amountPaid / $cents, $decimals);

        $order = $this->helper->loadOrderByIncrementId($orderId);

        $items = $order->getAllItems();

        foreach ($items as $item)
        {
            if ($item->getProductId() == $productId)
            {
                $item->setQtyInvoiced(0);
                $item->setQtyCanceled(0);

                $orderItemId = $item->getId();
                $orderItemQty = $item->getQtyOrdered();
                $taxAmount = $item->getTaxAmount();
                $baseTaxAmount = $item->getBaseTaxAmount();
                $grandTotal = $item->getRowTotalInclTax();
                $baseGrandTotal = $item->getBaseRowTotalInclTax();
                $subTotal = $item->getRowTotal();
                $baseSubtotal = $item->getBaseRowTotal();

                break;
            }
        }

        // Scenario where the merchant switched the customer to another subscription plan
        // In theory here's how to handle this scenario, but in practice, the invoice must be created first
        // We'd probably be better off if we created a new order with this product
        if (!$orderItemId)
        {
            // $item = $this->objectManager->create('Magento\Sales\Model\Order\Invoice\Item');
            // $orderItemQty = $subscription->quantity;
            // $taxAmount = 0;
            // $baseTaxAmount = 0;
            // $grandTotal = $amountPaid;
            // $baseGrandTotal = 0;
            // $subTotal = $amountPaid;
            // $baseSubtotal = 0;

            // $planAmount = round($subscription->plan->amount / $cents, $decimals);

            // $item->setName($subscription->plan->name);
            // $item->setQtyOrdered($orderItemQty);
            // $item->setPrice($planAmount);
            // $item->save();
            // $orderItemId = $item->getId();
            throw new \Exception("Could not match the product ID $productId with an item on order #$orderId - ignoring.");
        }

        $discount = $grandTotal - $amountPaid;
        $baseDiscount = $discount * ($baseGrandTotal / $grandTotal);

        $itemsArray = array($orderItemId => $orderItemQty);

        $invoice = $this->invoiceService->prepareInvoice($order, $itemsArray);

        // There is only one order item per invoice
        foreach ($invoice->getAllItems() as $invoiceItem)
        {
            $invoiceItem->setRowTotal($subTotal);
            $invoiceItem->setBaseRowTotal($baseSubtotal);
            $invoiceItem->setSubtotal($grandTotal);
            $invoiceItem->setBaseSubtotal($baseGrandTotal);
            $invoiceItem->setTaxAmount($taxAmount);
            $invoiceItem->setBaseTaxAmount($baseTaxAmount);
            $invoiceItem->setDiscountAmount($discount);
            $invoiceItem->setBaseDiscountAmount($baseDiscount);
        }
        $invoice->setTaxAmount($taxAmount);
        $invoice->setBaseTaxAmount($baseTaxAmount);
        // $invoice->setDiscountTaxCompensationAmount()
        // $invoice->setBaseDiscountTaxCompensationAmount()
        // $invoice->setShippingTaxAmount();
        // $invoice->setBaseShippingTaxAmount();
        $invoice->setShippingAmount(0); // Shipping should be included in the subscription price
        $invoice->setBaseShippingAmount(0);
        $invoice->setDiscountAmount($discount);
        $invoice->setBaseDiscountAmount($baseDiscount);
        // $invoice->setBaseCost();
        $invoice->setSubtotal($subTotal);
        $invoice->setBaseSubtotal($baseSubtotal);
        $invoice->setGrandTotal($grandTotal - $discount);
        $invoice->setBaseGrandTotal($baseGrandTotal - $baseDiscount);

        $invoice->setTransactionId($transactionId);
        if ($markAsPaid)
        {
            $invoice->setState($invoice::STATE_PAID);
        }
        else
        {
            $invoice->setState($invoice::STATE_OPEN);
            $invoice->setRequestedCaptureCase($invoice::CAPTURE_OFFLINE);
        }
        // Invoice notification to the customer
        $notifyCustomerByEmail = true;
        $visibleOnFront = true;

        $invoice->register();

        $transactionSave = $this->dbTransaction;
        $transactionSave->addObject($invoice);
        $transactionSave->addObject($invoice->getOrder());
        $transactionSave->save();

        $comment = $this->helper->createInvoiceComment("Created invoice for order #$orderId based on subscription", $notifyCustomerByEmail, $visibleOnFront);
        $comment->setInvoice($invoice);
        $comment->setParentId($invoice->getId());
        $comment->save();

        $invoice->addComment($comment, $notifyCustomerByEmail, $visibleOnFront);

        $order->addStatusHistoryComment(__('Notified customer about invoice #%1.', $invoice->getId()))
            ->setIsCustomerNotified(true)
            ->save();
    }

    public function execute()
    {
        try
        {
            // Retrieve the request's body and parse it as JSON
            $body = @file_get_contents('php://input');
            $event = json_decode($body);

            switch ($event->type)
            {
                case 'invoice.payment_succeeded':
                    $this->paymentSucceeded($event);
                    break;
                case 'invoice.payment_failed':
                    //$this->paymentFailed($event);
                    break;
                default:
                    return false;
            }
        }
        catch (\Exception $e)
        {
            $this->helper->dieWithError($e->getMessage(), $e);
        }
    }
}