<?php
/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

namespace Cryozonic\StripePayments\Model;

require_once dirname(__DIR__) . "/lib/autoload.php";

use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Command\CommandPoolInterface;
use Magento\Payment\Gateway\Config\ValueHandlerPoolInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectFactory;
use Magento\Payment\Gateway\Validator\ValidatorPoolInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Api\Data\CartInterface;
use Cryozonic\StripePayments\Helper;
use Psr\Log\LoggerInterface;
use Magento\Framework\Validator\Exception;
use Cryozonic\StripePayments\Helper\Logger;

class PaymentMethod extends \Magento\Payment\Model\Method\Adapter
{
    public static $code                 = "cryozonic_stripe";
    public static $moduleName           = "Stripe Payments M2";
    public static $moduleVersion        = "1.4.1";

    protected $_isInitializeNeeded      = false;
    protected $_canUseForMultishipping  = false;

    /**
     * @param ManagerInterface $eventManager
     * @param ValueHandlerPoolInterface $valueHandlerPool
     * @param PaymentDataObjectFactory $paymentDataObjectFactory
     * @param string $code
     * @param string $formBlockType
     * @param string $infoBlockType
     * @param Cryozonic\StripePayments\Model\Config $config
     * @param CommandPoolInterface $commandPool
     * @param ValidatorPoolInterface $validatorPool
     */
    public function __construct(
        ManagerInterface $eventManager,
        ValueHandlerPoolInterface $valueHandlerPool,
        PaymentDataObjectFactory $paymentDataObjectFactory,
        $code,
        $formBlockType,
        $infoBlockType,
        \Cryozonic\StripePayments\Model\Config $config,
        \Cryozonic\StripePayments\Helper\Generic $helper,
        \Cryozonic\StripePayments\Helper\Api $api,
        \Cryozonic\StripePayments\Model\StripeCustomer $customer,
        LoggerInterface $logger,
        CommandPoolInterface $commandPool = null,
        ValidatorPoolInterface $validatorPool = null
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->api = $api;
        $this->customer = $customer;
        $this->saveCards = $config->getSaveCards();

        \Stripe\Stripe::setApiKey($config->getSecretKey());

        parent::__construct(
            $eventManager,
            $valueHandlerPool,
            $paymentDataObjectFactory,
            $code,
            $formBlockType,
            $infoBlockType,
            $commandPool,
            $validatorPool
        );
    }

    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        // From Magento 2.0.7 onwards, the data is passed in a different property
        $additionalData = $data->getAdditionalData();
        if (is_array($additionalData))
            $data->setData(array_merge($data->getData(), $additionalData));

        $info = $this->getInfoInstance();

        if ($this->helper->isAdminSubscriptionSwitch($data))
        {
            $info->setAdditionalInformation('switch_subscription', $data['subscription']);
            return $this;
        }
        else if ($info->getAdditionalInformation('switch_subscription'))
        {
            $info->setAdditionalInformation('switch_subscription', null);
        }

        if (!empty($data['cc_saved']) && $data['cc_saved'] != 'new_card')
        {
            $card = explode(':', $data['cc_saved']);
            $data['cc_saved'] = $card[0]; // To be used by Stripe Subscriptions
            $info->setAdditionalInformation('token', $card[0])
                ->setCcType($card[1])
                ->setCcLast4($card[2]);
            return $this;
        }

        if (empty($data['cc_stripejs_token']) && empty($data['cc_number']))
            return $this;

        if (!empty($data['cc_stripejs_token']))
        {
            $card = explode(':', $data['cc_stripejs_token']);
            $data['cc_stripejs_token'] = $card[0]; // To be used by Stripe Subscriptions

            // This is called both at the card filling step and also at the final step, so add some safety measures
            $usedToken = $info->getAdditionalInformation('stripejs_token');

            if (!empty($usedToken) && $usedToken == $card[0])
                return $this;

            // What to do at the card filling step
            $params = array(
                "card" => $card[0]
            );
            $info->setAdditionalInformation('stripejs_token', $card[0])
                ->setCcType($card[1])
                ->setCcLast4($card[2]);
        }
        else
            $params = [
                "card" => [
                    "name" => $data['cc_owner'],
                    "number" => $data['cc_number'],
                    "cvc" => $data['cc_cid'],
                    "exp_month" => $data['cc_exp_month'],
                    "exp_year" => $data['cc_exp_year']
                ]
            ];

        // Add the card to the customer
        if ($this->config->getSaveCards() && $data['cc_save'])
        {
            try
            {
                $card = $this->customer->addSavedCard($params['card']);
                $token = $card->id;
            }
            catch (\Stripe\Error $e)
            {
                $this->helper->dieWithError($e->getMessage(), $e);
            }
            catch (\Stripe\Error\Card $e)
            {
                $this->helper->dieWithError($e->getMessage());
            }
            catch (\Exception $e)
            {
                $this->helper->dieWithError("An error has occured. Please contact us to complete your order.", $e);
            }
        }
        else
            $token = $this->api->createToken($params);

        $info->setAdditionalInformation('token', $token);

        if (isset($this->customer->customerCard))
        {
            $info->setCcType($this->customer->customerCard['brand'])
                ->setCcLast4($this->customer->customerCard['last4']);
        }

        return $this;
    }

    public function authorize(InfoInterface $payment, $amount)
    {
        if ($amount > 0)
        {
            $this->api->createCharge($payment, $amount, false);
        }

        return $this;
    }

    public function capture(InfoInterface $payment, $amount)
    {
        if ($amount > 0)
        {
            $captured = $payment->getAdditionalInformation('captured');
            if ($this->helper->isAdmin() && $captured === false)
            {
                // We are in the admin area trying to capture an Authorize Only order
                $token = $payment->getTransactionId();
                $token = preg_replace('/-.*$/', '', $token);
                try
                {
                    $ch = \Stripe\Charge::retrieve($token);

                    if ($this->config->useStoreCurrency())
                        $finalAmount = $this->helper->getMultiCurrencyAmount($payment, $amount);
                    else
                        $finalAmount = $amount;

                    $currency = $payment->getOrder()->getOrderCurrencyCode();
                    $cents = 100;
                    if ($this->helper->isZeroDecimal($currency))
                        $cents = 1;

                    $ch->capture(array('amount' => round($finalAmount * $cents)));
                }
                catch (\Exception $e)
                {
                    $this->logger->critical($e->getMessage());

                    if ($this->helper->isAuthorizationExpired($e->getMessage()) && $this->config->retryWithSavedCard())
                        $this->api->createCharge($payment, $amount, true, true);
                    else
                        $this->helper->dieWithError($e->getMessage(), $e);
                }
            }
            else
            {
                // Normal checkout payments in Authorize & Capture mode
                $this->api->createCharge($payment, $amount, true);
            }
        }

        return $this;
    }

    public function cancel(InfoInterface $payment, $amount = null)
    {
        if ($this->config->useStoreCurrency())
        {
            // Captured
            $creditmemo = $payment->getCreditmemo();
            if (!empty($creditmemo))
            {
                $rate = $creditmemo->getBaseToOrderRate();
                if (!empty($rate) && is_numeric($rate) && $rate > 0)
                    $amount *= $rate;
            }
            // Authorized
            $amount = (empty($amount)) ? $payment->getOrder()->getTotalDue() : $amount;

            $currency = $payment->getOrder()->getOrderCurrencyCode();
        }
        else
        {
            // Authorized
            $amount = (empty($amount)) ? $payment->getOrder()->getBaseTotalDue() : $amount;

            $currency = $payment->getOrder()->getBaseCurrencyCode();
        }

        $transactionId = $payment->getParentTransactionId();
        $transactionId = preg_replace('/-.*$/', '', $transactionId);

        try {
            $cents = 100;
            if ($this->helper->isZeroDecimal($currency))
                $cents = 1;

            $params = array();
            if ($amount > 0)
                $params["amount"] = round($amount * $cents);

            $charge = \Stripe\Charge::retrieve($transactionId);

            // This is true when an authorization has expired or when there was a refund through the Stripe account
            if (!$charge->refunded)
            {
                $charge->refund($params);
                // \Stripe\Refund::create($params);

                $payment->getOrder()->addStatusToHistory(
                    \Magento\Sales\Model\Order::STATE_CANCELED,
                    __('Customer was refunded the amount of '). $amount
                );
            }
            else
            {
                $msg = __('This order has already been refunded in Stripe. To refund from Magento, please refund it offline.');
                $this->helper->addError($msg);
                throw new LocalizedException($msg);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->addError('Could not refund payment: '.$e->getMessage());
            throw new \Exception(__($e->getMessage()));
        }

        return $this;
    }

    public function refund(InfoInterface $payment, $amount)
    {
        $this->cancel($payment, $amount);

        return $this;
    }

    public function void(InfoInterface $payment)
    {
        $this->cancel($payment);

        return $this;
    }

    public function acceptPayment(InfoInterface $payment)
    {
        return parent::acceptPayment($payment);
    }

    public function denyPayment(InfoInterface $payment)
    {
        return parent::denyPayment($payment);
    }

    // Fixes https://github.com/magento/magento2/issues/5413 in Magento 2.1
    public function setId($code) { }
    public function getId() { return $this::$code; }

    public static function module()
    {
        return PaymentMethod::$moduleName . " v" . PaymentMethod::$moduleVersion;
    }
}