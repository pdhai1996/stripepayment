<?php
/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */

namespace Cryozonic\StripePayments\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Backend\Model\Session;
use Cryozonic\StripePayments\Model;
use Psr\Log\LoggerInterface;
use Magento\Framework\Validator\Exception;
use Cryozonic\StripePayments\Helper\Logger;
use Cryozonic\StripePayments\Model\PaymentMethod;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Generic
{
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session\Quote $sessionQuote,
        \Magento\Framework\App\Request\Http $request,
        LoggerInterface $logger,
        \Magento\Framework\App\State $appState,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Invoice $invoice,
        \Magento\Sales\Model\Order\Creditmemo $creditmemo,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Block\Adminhtml\Order\Create\Form\Address $adminOrderAddressForm,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Sales\Model\Order\Invoice\CommentFactory $invoiceCommentFactory,
        \Magento\Customer\Model\Address $customerAddress,
        \Magento\Framework\Webapi\Response $apiResponse
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->sessionQuote = $sessionQuote;
        $this->request = $request;
        $this->logger = $logger;
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->order = $order;
        $this->invoice = $invoice;
        $this->creditmemo = $creditmemo;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->resource = $resource;
        $this->coreRegistry = $coreRegistry;
        $this->adminOrderAddressForm = $adminOrderAddressForm;
        $this->customerRegistry = $customerRegistry;
        $this->messageManager = $messageManager;
        $this->productModel = $productModel;
        $this->orderModel = $orderModel;
        $this->cart = $cart;
        $this->priceCurrency = $priceCurrency;
        $this->invoiceCommentFactory = $invoiceCommentFactory;
        $this->customerAddress = $customerAddress;
        $this->apiResponse = $apiResponse;
    }

    public function getSessionQuote()
    {
        return $this->sessionQuote;
    }

    public function getStoreId()
    {
        if ($this->isAdmin())
        {
            if ($this->request->getParam('order_id', null))
            {
                // Viewing an order
                $order = $this->order->load($this->request->getParam('order_id', null));
                return $order->getStoreId();
            }
            if ($this->request->getParam('invoice_id', null))
            {
                // Viewing an invoice
                $invoice = $this->invoice->load($this->request->getParam('invoice_id', null));
                return $invoice->getStoreId();
            }
            else if ($this->request->getParam('creditmemo_id', null))
            {
                // Viewing a credit memo
                $creditmemo = $this->creditmemo->load($this->request->getParam('creditmemo_id', null));
                return $creditmemo->getStoreId();
            }
            else
            {
                // Creating a new order
                $quote = $this->getSessionQuote();
                return $quote->getStoreId();
            }
        }
        else
        {
            return $this->storeManager->getStore()->getId();
        }
    }

    public function loadProductById($productId)
    {
        $this->productModel->clearInstance();
        return $this->productModel->load($productId);
    }

    public function loadOrderByIncrementId($incrementId)
    {
        $this->orderModel->clearInstance();
        return $this->orderModel->loadByIncrementId($incrementId);
    }

    public function createInvoiceComment($msg, $notify = false, $visibleOnFront = false)
    {
        return $this->invoiceCommentFactory->create()
            ->setComment($msg)
            ->setIsCustomerNotified($notify)
            ->setIsVisibleOnFront($visibleOnFront);
    }

    public function isAdmin()
    {
        $areaCode = $this->appState->getAreaCode();

        return $areaCode == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE;
    }

    public function getCustomerId()
    {
        // If we are in the back office
        if ($this->isAdmin())
        {
            // About to refund/invoice an order
            if ($order = $this->coreRegistry->registry('current_order'))
                return $order->getCustomerId();

            // About to capture an invoice
            if ($invoice = $this->coreRegistry->registry('current_invoice'))
                return $invoice->getCustomerId();

            // Creating a new order from admin
            if ($this->adminOrderAddressForm && $this->adminOrderAddressForm->getCustomerId())
                return $this->adminOrderAddressForm->getCustomerId();
        }
        // If we are on the checkout page
        else if ($this->customerSession->isLoggedIn())
        {
            return $this->customerSession->getCustomerId();
        }

        return null;
    }

    public function getMagentoCustomer()
    {
        if ($this->customerSession->getCustomer()->getEntityId())
            return $this->customerSession->getCustomer();

        $customerId = $this->getCustomerId();
        if (!$customerId) return;

        $customer = $this->customerRegistry->retrieve($customerId);

        if ($customer->getEntityId())
            return $customer;

        return null;
    }

    // Should return the email address of guest customers
    public function getCustomerEmail()
    {
        $customer = $this->getMagentoCustomer();

        if (!$customer)
            $customer = $this->getGuestCustomer();

        if (!$customer)
            return null;

        return trim(strtolower($customer->getEmail()));
    }

    public function getGuestCustomer($order = null)
    {
        if ($order)
        {
            return $this->getAddressFrom($order, 'billing');
        }
        else if (isset($this->_order))
        {
            return $this->getAddressFrom($this->_order, 'billing');
        }
        else
            return null;
    }

    public function getCustomerBillingAddress()
    {
        $customer = $this->getMagentoCustomer();
        if (!$customer) return null;

        $addressId = $customer->getDefaultBilling();
        if (!$addressId) return null;

        $this->customerAddress->clearInstance();
        $address = $this->customerAddress->load($addressId);
        return $address;
    }

    public function getStripeFormattedBillingAddress()
    {
        $address = $this->getCustomerBillingAddress();

        if (empty($address))
            return null;

        return [
            "address_line1" => $address->getStreetLine(1),
            "address_line2" => $address->getStreetLine(2),
            "address_city" => $address->getCity(),
            "address_state" => $address->getRegion(),
            "address_zip" => $address->getPostcode(),
            "address_country" => $address->getCountryId()
        ];
    }

    public function getCustomerLastRetrieved()
    {
        if (isset($this->customerLastRetrieved))
            return $this->customerLastRetrieved;

        // Get the magento customer id
        if (empty($customerId))
            $customerId = $this->getCustomerId();

        if (!empty($customerId) && $customerId < 1)
            $customerId = null;

        $connection = $this->resource->getConnection('core_read');
        $query = $connection->select()
            ->from('cryozonic_stripe_customers', ['*'])
            ->where('customer_id=?', $customerId);

        $result = $connection->fetchRow($query);
        if (empty($result)) return false;
        $this->customerStripeId = $result['stripe_id'];
        return $this->customerLastRetrieved = $result['last_retrieved'];
    }

    public function getAvsFields($card)
    {
        // If the card is a token then AVS should have already been taken care of during token creation
        if (!is_array($card))
            return $card;

        $address = $this->getStripeFormattedBillingAddress();

        // Union the arrays, existing keys from $card are preserved
        if (is_array($address))
            return $card + $address;

        return $card;
    }

    protected function updateLastRetrieved($stripeCustomerId)
    {
        try
        {
            $connection = $this->resource->getConnection('core_write');
            $fields = array();
            $fields['last_retrieved'] = time();
            $condition = array($connection->quoteInto('stripe_id=?', $stripeCustomerId));
            $result = $connection->update('cryozonic_stripe_customers', $fields, $condition);
        }
        catch (\Exception $e)
        {
            $this->logger->addError('Could not update Stripe customers table: '.$e->getMessage());
        }
    }

    public function getMultiCurrencyAmount($payment, $baseAmount)
    {
        $order = $payment->getOrder();
        $grandTotal = $order->getGrandTotal();
        $baseGrandTotal = $order->getBaseGrandTotal();

        $rate = $order->getBaseToOrderRate();
        if ($rate == 0) $rate = 1;

        // Full capture, ignore currency rate in case it changed
        if ($baseAmount == $baseGrandTotal)
            return $grandTotal;
        // Partial capture, consider currency rate but don't capture more than the original amount
        else if (is_numeric($rate))
            return min($baseAmount * $rate, $grandTotal);
        // Not a multicurrency capture
        else
            return $baseAmount;
    }

    public function getAddressFrom($order, $addressType = 'shipping')
    {
        if (!$order) return null;

        $addresses = $order->getAddresses();
        foreach ($addresses as $address)
        {
            if ($address["address_type"] == $addressType)
                return $address;
        }

        return null;
    }

    public function isFrontEndSubscriptionPurchase()
    {
        return !$this->isAdmin() && $this->hasSubscriptions();
    }

    public function hasSubscriptions()
    {
        if (isset($this->_hasSubscriptions))
            return true;

        $items = $this->cart->getQuote()->getAllItems();
        foreach ($items as $item)
        {
            $product = $this->loadProductById($item->getProduct()->getEntityId());
            if ($product->getCryozonicSubEnabled())
                return $this->_hasSubscriptions = true;
        }
    }

    public function isAdminSubscriptionSwitch($data)
    {
        return (is_array($data['subscription']) &&
            isset($data['subscription']['switch']) &&
            $data['subscription']['switch'] == 'switch');
    }

    public function isZeroDecimal($currency)
    {
        return in_array(strtolower($currency), array(
            'bif', 'djf', 'jpy', 'krw', 'pyg', 'vnd', 'xaf',
            'xpf', 'clp', 'gnf', 'kmf', 'mga', 'rwf', 'vuv', 'xof'));
    }

    public function formatSubscriptionName($sub)
    {
        if (empty($sub)) return "Unknown subscription";

        $name = $sub->plan->name;

        $currency = $sub->plan->currency;
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION;
        $cents = 100;
        $qty = '';

        if ($this->isZeroDecimal($currency))
        {
            $cents = 1;
            $precision = 0;
        }

        $amount = $sub->plan->amount / $cents;

        if ($sub->quantity > 1)
        {
            $qty = " x " . $sub->quantity;
        }

        $this->priceCurrency->getCurrency()->setCurrencyCode($currency);
        $cost = $this->priceCurrency->format($amount, false, $precision);

        return "$name ($cost$qty)";
    }

    public function isAuthorizationExpired($errorMessage)
    {
        return (strstr($errorMessage, "cannot be captured because the charge has expired") !== false);
    }

    public function addError($msg)
    {
        $this->messageManager->addError( __($msg) );
    }

    public function addSuccess($msg)
    {
        $this->messageManager->addSuccess( __($msg) );
    }

    public function logError($msg)
    {
        $this->logger->addError(PaymentMethod::module() . ": " . $msg);
    }

    public function dieWithError($msg, $e = null)
    {
        $this->logError($msg);

        if ($e)
        {
            if ($e->getMessage() != $msg)
                $this->logError($e->getMessage());

            $this->logError($e->getTraceAsString());
        }

        if ($this->isAdmin())
        {
            $this->addError($msg);
            throw new \Exception($msg);
        }
        else
        {
            // The checkout page uses the Magento's Web API, which has an open bug https://github.com/magento/magento2/issues/6929
            $this->apiResponse->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->apiResponse->setBody(json_encode(array('message' => $msg)));
            $this->apiResponse->sendResponse();
            die; // We die here to avoid receiving a generic error message and instead get a user friendly error at the front-end
        }
    }
}