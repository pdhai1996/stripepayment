/**
 * Copyright © Cryozonic Ltd. All rights reserved.
 *
 * @package    Cryozonic_StripePayments
 * @copyright  Copyright © Cryozonic Ltd (http://cryozonic.com)
 * @license    Commercial (See http://cryozonic.com/licenses/stripe.html for details)
 */
/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Ui/js/model/messageList',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'jquery'
    ],
    function (ko, Component, globalMessageList, quote, customer, $) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cryozonic_StripePayments/payment/form',
                cryozonicStripeCardSave: true,
                cryozonicStripeShowApplePaySection: false,
                cryozonicApplePayToken: null
            },

            initObservable: function ()
            {
                this._super()
                    .observe([
                        'cryozonicStripeCardName',
                        'cryozonicStripeCardNumber',
                        'cryozonicStripeCardExpMonth',
                        'cryozonicStripeCardExpYear',
                        'cryozonicStripeCardVerificationNumber',
                        'cryozonicStripeJsToken',
                        'cryozonicApplePayToken',
                        'cryozonicStripeCardSave',
                        'cryozonicStripeSelectedCard',
                        'cryozonicStripeShowNewCardSection',
                        'cryozonicStripeShowApplePaySection',
                        'cryozonicApplePayToken'
                    ]);

                this.cryozonicStripeSelectedCard.subscribe(this.onSelectedCardChanged, this);
                this.cryozonicStripeSelectedCard('new_card');
                if (!this.config().savedCards.length)
                    this.cryozonicStripeShowNewCardSection(true);

                var self = this;

                if (this.config().isStripeJsEnabled)
                {
                    initStripe(this.config().stripeJsKey, function(err)
                    {
                        self.onStripeJsInitialized.call(self, err);
                    });
                }

                this.showSavedCardsSection = ko.computed(function()
                {
                    return this.config().savedCards.length && this.isBillingAddressSet() && !this.cryozonicApplePayToken();
                }, this);

                this.showNewCardSection = ko.computed(function()
                {
                    return this.cryozonicStripeShowNewCardSection() && this.isBillingAddressSet() && !this.cryozonicApplePayToken();
                }, this);

                this.showSaveCardOption = ko.computed(function()
                {
                    return this.config().showSaveCardOption && customer.isLoggedIn() && (this.showNewCardSection() || this.cryozonicApplePayToken());
                }, this);

                return this;
            },

            onSelectedCardChanged: function(newValue)
            {
                if (newValue == 'new_card')
                    this.cryozonicStripeShowNewCardSection(true);
                else
                    this.cryozonicStripeShowNewCardSection(false);
            },

            onStripeJsInitialized: function(err)
            {
                if (err)
                {
                    console.warn("Stripe.js could not be loaded: " + err);
                    return;
                }

                if (this.isApplePayEnabled())
                {
                    var self = this;
                    Stripe.applePay.checkAvailability(function(available)
                    {
                        if (available)
                            self.cryozonicStripeShowApplePaySection(true);
                    });
                }
            },

            isBillingAddressSet: function()
            {
                return quote.billingAddress() && quote.billingAddress().canUseForBilling();
            },

            isPlaceOrderEnabled: function()
            {
                if (this.isBillingAddressSet())
                    cryozonic.setAVSFieldsFrom(quote.billingAddress());

                return this.isBillingAddressSet();
            },

            isApplePayEnabled: function()
            {
                return this.config().isStripeJsEnabled && this.getApplePayParams();
            },

            getApplePayParams: function()
            {
                if (!this.isBillingAddressSet())
                    return null;

                var amount, currency;
                if (this.config().useStoreCurrency)
                {
                    currency = quote.totals().quote_currency_code;
                    amount = quote.totals().grand_total;
                }
                else
                {
                    currency = quote.totals().base_currency_code;
                    amount = quote.totals().base_grand_total;
                }

                return {
                    "countryCode": quote.billingAddress().countryId,
                    "currencyCode": currency,
                    "total": {
                        "label": "Order by " + quote.billingAddress().firstname + " " + quote.billingAddress().lastname + " <" + customer.customerData.email + ">",
                        "amount": amount
                    }
                };
            },

            beginApplePay: function()
            {
                var self = this;
                var paymentRequest = this.getApplePayParams();
                var session = Stripe.applePay.buildSession(paymentRequest, function(result, completion)
                {
                    self.cryozonicApplePayToken(result.token);
                    self.cryozonicStripeShowApplePaySection(false);
                    self.cryozonicStripeJsToken(result.token.id + ':' + result.token.card.brand + ':' + result.token.card.last4);
                    completion(ApplePaySession.STATUS_SUCCESS);
                },
                function(error)
                {
                    alert(error.message);
                });

                session.begin();
            },

            resetApplePay: function()
            {
                this.cryozonicApplePayToken(null);
                this.cryozonicStripeShowApplePaySection(true);
                this.cryozonicStripeJsToken(null);
            },

            config: function()
            {
                return window.checkoutConfig.payment[this.getCode()];
            },

            isActive: function(parents)
            {
                return true;
            },

            isNewCard: function()
            {
                if (!this.config().savedCards.length) return true;
                if (this.cryozonicStripeSelectedCard() == 'new_card') return true;
                return false;
            },

            stripeJsPlaceOrder: function()
            {
                if (this.cryozonicApplePayToken())
                {
                    this.placeOrder();
                }
                else if (this.config().isStripeJsEnabled)
                {
                    var self = this;

                    this.cryozonicStripeJsToken(null);

                    createStripeToken(function(err, token)
                    {
                        if (err)
                            return self.showError(err);
                        else
                        {
                            self.cryozonicStripeJsToken(token);
                            self.placeOrder();
                        }
                    });
                }
                else
                    this.placeOrder();
            },

            showError: function(message)
            {
                document.getElementById('checkout').scrollIntoView(true);
                globalMessageList.addErrorMessage({ "message": message });
            },

            afterPlaceOrder: function()
            {
            },

            validate: function(elm)
            {
                if (this.cryozonicApplePayToken)
                    return true;

                // @todo Replace these with proper form validation
                var data = this.getData().additional_data;

                if (this.isNewCard())
                {
                    if (this.config().isStripeJsEnabled)
                    {
                        if (!this.cryozonicStripeJsToken())
                            return this.showError('Could not process card details, please try again.');
                    }
                    else
                    {
                        if (!data.cc_owner) return this.showError('Please enter the cardholder name');
                        if (!data.cc_number) return this.showError('Please enter your card number');
                        if (!data.cc_exp_month) return this.showError('Please enter your card\'s expiration month');
                        if (!data.cc_exp_year) return this.showError('Please enter your card\'s expiration year');
                        if (!data.cc_cid) return this.showError('Please enter your card\'s security code (CVN)');
                    }
                }
                else if (!this.cryozonicStripeSelectedCard() || this.cryozonicStripeSelectedCard().indexOf('card_') !== 0)
                    return this.showError('Please select a card!');

                return true;
            },

            getCode: function()
            {
                return 'cryozonic_stripe';
            },

            getData: function()
            {
                var data = {
                    'method': this.item.method
                };

                if (this.cryozonicStripeSelectedCard() && this.cryozonicStripeSelectedCard().indexOf('card_') === 0)
                {
                    data.additional_data = {
                        'cc_saved': this.cryozonicStripeSelectedCard()
                    };
                }
                else if (this.config().isStripeJsEnabled)
                {
                    data.additional_data = {
                        'cc_stripejs_token': this.cryozonicStripeJsToken(),
                        'cc_save': this.showSaveCardOption() && this.cryozonicStripeCardSave()
                    };
                }
                else
                {
                    data.additional_data = {
                        'cc_owner': this.cryozonicStripeCardName(),
                        'cc_number': this.cryozonicStripeCardNumber(),
                        'cc_exp_month': this.cryozonicStripeCardExpMonth(),
                        'cc_exp_year': this.cryozonicStripeCardExpYear(),
                        'cc_cid': this.cryozonicStripeCardVerificationNumber(),
                        'cc_save': this.showSaveCardOption() && this.cryozonicStripeCardSave()
                    };
                }

                return data;
            },

            getCcMonthsValues: function() {
                return $.map(this.getCcMonths(), function(value, key) {
                    return {
                        'value': key,
                        'month': value
                    };
                });
            },

            getCcYearsValues: function() {
                return $.map(this.getCcYears(), function(value, key) {
                    return {
                        'value': key,
                        'year': value
                    };
                });
            },

            getCcMonths: function()
            {
                return window.checkoutConfig.payment[this.getCode()].months;
            },

            getCcYears: function()
            {
                return window.checkoutConfig.payment[this.getCode()].years;
            },

            getCvvImageUrl: function() {
                return window.checkoutConfig.payment[this.getCode()].cvvImageUrl;
            },

            getCvvImageHtml: function() {
                return '<img src="' + this.getCvvImageUrl() +
                    '" alt="' + 'Card Verification Number Visual Reference' +
                    '" title="' + 'Card Verification Number Visual Reference' +
                    '" />';
            }
        });
    }
);